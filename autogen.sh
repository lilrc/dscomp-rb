#!/bin/sh
#
#  This file is part of dscomp-rb.
#
#  Copyright (C) 2014 Karl Lindén <lilrc@users.sourceforge.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

# Usage: die <message to print>
die() {
	echo "${@}"
	exit 1
}

TOOLS="aclocal autoconf automake autoheader libtoolize"

for tool in ${TOOLS}
do
	echo -n "checking for ${tool}... "
	if command -v ${tool} > /dev/null 2>&1
	then
		echo "found"
	else
		echo "not found"
		echo "You do not have ${tool} correctly installed. You will not"
		echo "be able to build this package without it."
		die "${tool} not found"
	fi
done

aclocal || die "aclocal failec"
libtoolize || die "libtoolize failed"
autoheader || die "autoheader failed"
automake --add-missing || die "automake failed"
autoconf || die "autoconf failed"

echo "You can now run:"
echo "    ./configure"
echo "    make"
echo "    make install"

exit 0
