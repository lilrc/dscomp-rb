/*
 * This file is part of dscomp-rb.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#ifndef RB_H
# define RB_H

# if HAVE_CONFIG_H
#  include <config.h>
# endif /* HAVE_CONFIG_H */

/* size_t */
# if HAVE_STDDEF_H
#  include <stddef.h>
# endif /* HAVE_STDDEF_H */

/* Define to recycle destroyed nodes to avoid unnecessary calls to
 * malloc(). */
# if !defined(RECYCLE)
#  define RECYCLE 0
# endif /* !defined(RECYCLE) */

/* Select a default implementation if none was requested. */
# if !PARENT && !RECURSIVE && !STACK
#  define RECURSIVE 1
# elif PARENT && RECURSIVE || PARENT && STACK || RECURSIVE && STACK
#  error Only one of PARENT, RECURSIVE and STACK are allowed at once.
# endif

enum rb_error_e {
	RB_OK = 0,
	RB_EXISTS,
	RB_NOMEM
};
typedef enum rb_error_e rb_error_t;

enum rb_colour_e {
	RB_RED,
	RB_BLACK
};
typedef enum rb_colour_e rb_colour_t;

struct rb_node_s {
	int key;
	int value;

	rb_colour_t colour;

	struct rb_node_s * left;
	struct rb_node_s * right;
# if PARENT
	struct rb_node_s * parent;
# endif /* PARENT */
};
typedef struct rb_node_s rb_node_t;

struct rb_s {
	rb_node_t * root;
# if RECYCLE
	rb_node_t * recycled;
# endif /* RECYCLE */

# if STACK
	rb_node_t ** stack;
	size_t stack_len;
	size_t stack_size;
# endif /* STACK */
};
typedef struct rb_s rb_t;

rb_t * rb_new ( void );
void rb_destroy ( rb_t * const rb )
	__attribute__((nonnull));

void rb_clear ( rb_t * const rb )
	__attribute__((nonnull));

rb_error_t rb_insert ( rb_t * const rb,
                       const int key,
                       const int value )
	__attribute__((nonnull));

int rb_search ( const rb_t * const rb,
                const int key,
                int * const valuep )
	__attribute__((nonnull));

int rb_check ( const rb_t * const rb ) __attribute__((nonnull));

#endif /* !RB_H */
