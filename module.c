/*
 * This file is part of dscomp-rb.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#include <dscomp-module.h>

#include "rb.h"

static void *
new_func ( void )
{
	rb_t * const rb = rb_new();
	return rb;
}

static void __attribute__((nonnull))
destroy_func ( void * const rb )
{
	rb_destroy(rb);
	return;
}

static int __attribute__((nonnull))
insert_func ( void * const rb, const int key, const int value )
{
	const rb_error_t ret = rb_insert(rb, key, value);
	if ( ret == RB_OK ) {
		return DSCOMP_OK;
	} else if ( ret == RB_EXISTS ) {
		return DSCOMP_EXITSTS;
	} else {
		return DSCOMP_ERROR;
	}
}

static int __attribute__((nonnull))
search_func ( void * const rb, const int key, int * const valuep )
{
	if ( rb_search(rb, key, valuep) ) {
		return DSCOMP_NOT_FOUND;
	}
	return DSCOMP_OK;
}

static int __attribute__((nonnull))
clear_func ( void * const rb )
{
	rb_clear(rb);
	return DSCOMP_OK;
}

static int __attribute__((nonnull))
check_func ( void * const rb )
{
	if ( rb_check(rb) ) {
		return DSCOMP_ERROR;
	}
	return DSCOMP_OK;
}

#if RECYCLE
# if PARENT
#  define module rb_parent_recycle_LTX_module
# elif RECURSIVE
#  define module rb_recursive_recycle_LTX_module
# elif STACK
#  define module rb_stack_recycle_LTX_module
# endif /* STACK */
#else /* !RECYCLE */
# if PARENT
#  define module rb_parent_LTX_module
# elif RECURSIVE
#  define module rb_recursive_LTX_module
# elif STACK
#  define module rb_stack_LTX_module
# endif /* STACK */
#endif /* !RECYCLE */

const dscomp_module_t module = {
	&new_func,
	&destroy_func,
	&insert_func,
	&search_func,
	&clear_func,
	&check_func
};
