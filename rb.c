/*
 * This file is part of dscomp-rb.
 * 
 * Copyright 2014 Karl Lindén <lilrc@users.sourceforge.net>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

/* Red-black binary search trees fulfil four rules, apart for those that
 * apply to all binary trees:
 * (1) A node is either red or black.
 * (2) The root node is black.
 * (3) All leaves (NULL) are black.
 * (4) A red node only has black children.
 * (5) Every path from a given node to any of its descendant leaves
 *     contains the same number of black nodes. */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

/* assert() */
#include <assert.h>

/* true */
#if HAVE_STDBOOL_H
# include <stdbool.h>
#endif /* HAVE_STDBOOL_H */

/* SIZE_MAX */
#include <stdint.h>

/* free(), malloc() */
#include <stdlib.h>

/* fprintf(), fputs(), perror(), stderr */
#include <stdio.h>

#include "rb.h"

#if RECYCLE
# define rb_node_remove(rb, node) rb_node_recycle(rb, node)
#else /* !RECYCLE */
# define rb_node_remove(rb, node) rb_node_destroy(node)
#endif /* !RECYCLE */

static inline void __attribute__((nonnull))
rb_node_init ( rb_node_t * const node,
               const int key,
               const int value )
{
	node->key = key;
	node->value = value;
	node->left = NULL;
	node->right = NULL;

	/* node->colour and node->parent do not need to be set. They will be
	 * set upon insertion. */

	return;
}

static inline rb_node_t * __attribute__((nonnull))
rb_node_new ( rb_t * const rb,
              const int key,
              const int value )
{
#if RECYCLE
	if ( rb->recycled != NULL ) {
		rb_node_t * const node = rb->recycled;
		rb->recycled = node->right;
		rb_node_init(node, key, value);
		return node;
	}
#endif /* RECYCLE */

	rb_node_t * const node = malloc(sizeof(rb_node_t));
	if ( node == NULL ) {
		perror("malloc");
		return NULL;
	}
	rb_node_init(node, key, value);
	return node;
}

static void __attribute__((nonnull))
rb_node_destroy ( rb_node_t * const node )
{
	free(node);
	return;
}

#if RECYCLE
static void __attribute__((nonnull))
rb_node_recycle ( rb_t * const rb,
                  rb_node_t * const node )
{
	node->right = rb->recycled;
	rb->recycled = node;
	return;
}
#endif /* RECYCLE */

static void __attribute__((nonnull))
rb_node_remove_recursive ( rb_t * const rb,
                           rb_node_t * const node )
{
	if ( node->left != NULL ) {
		rb_node_remove_recursive(rb, node->left);
	}
	if ( node->right != NULL ) {
		rb_node_remove_recursive(rb, node->right);
	}
	rb_node_remove(rb, node);
	return;
}

rb_t *
rb_new ( void )
{
	rb_t * const rb = malloc(sizeof(rb_t));
	if ( rb == NULL ) {
		perror("malloc");
		return NULL;
	}

	rb->root = NULL;

#if RECYCLE
	rb->recycled = NULL;
#endif /* RECYCLE */

#if STACK
	rb->stack = NULL;
	/* There is no need to set stack_len here as it is set by
	 * rb_stack_clear() before the stack is used. */
	/* rb->stack_len = 0; */
	rb->stack_size = 0;
#endif /* STACK */

	return rb;
}

void __attribute__((nonnull))
rb_destroy ( rb_t * const rb )
{
	rb_clear(rb);

#if RECYCLE
	rb_node_t * node = rb->recycled;
	while ( node != NULL ) {
		rb_node_t * const next = node->right;
		rb_node_destroy(node);
		node = next;
	}
#endif /* RECYCLE */

#if STACK
	free(rb->stack);
#endif /* STACK */

	free(rb);
	return;
}

void __attribute__((nonnull))
rb_clear ( rb_t * const rb )
{
	if ( rb->root != NULL ) {
		rb_node_remove_recursive(rb, rb->root);
		rb->root = NULL;
	}
	return;
}

#if STACK
static int __attribute__((nonnull))
rb_stack_push ( rb_t * const rb, rb_node_t * const node )
{
	if ( rb->stack_len == rb->stack_size ) {
		rb->stack_size++;
		rb->stack = realloc(rb->stack,
		                    rb->stack_size * sizeof(rb_node_t *));
		if ( rb->stack == NULL ) {
			perror("realloc");
			return 1;
		}
	}

	rb->stack[rb->stack_len] = node;
	rb->stack_len++;
	return 0;
}

static rb_node_t * __attribute__((nonnull))
rb_stack_pop ( rb_t * const rb )
{
	if ( rb->stack_len == 0 ) {
		return NULL;
	}

	rb->stack_len--;
	return rb->stack[rb->stack_len];
}

static inline void __attribute__((nonnull))
rb_stack_clear ( rb_t * const rb )
{
	rb->stack_len = 0;
	return;
}
#endif /* STACK */

static rb_node_t * __attribute__((nonnull))
rb_node_get_uncle ( const rb_node_t * const grand,
                    const rb_node_t * const parent )
{
	assert(parent == grand->left || parent == grand->right);
	if ( parent == grand->left ) {
		return grand->right;
	} else {
		return grand->left;
	}
}

/* This function performs a right rotation on the red-black tree.
 * Node must be red and must also the left child of parent.
 * Parent must be red and must also be the left child of grand.
 * The uncle must be black.
 * Grandparent must be black.
 * The function returns the new grandparent. */
static rb_node_t * __attribute__((nonnull))
rb_right_rotation ( rb_node_t * const grand,
                    rb_node_t * const parent )
{
	/* Assert all necessary properties of the given. */
	assert(parent->colour == RB_RED);
	assert(grand->colour == RB_BLACK);
	assert(grand->left == parent);

	/* Assert properties of the node and the uncle. */
	assert(parent->left->colour == RB_RED);
	assert(grand->right == NULL || grand->right->colour == RB_BLACK);

	grand->colour = RB_RED;
	parent->colour = RB_BLACK;

	grand->left = parent->right;
	parent->right = grand;

#if PARENT
	parent->parent = grand->parent;
	grand->parent = parent;
	if ( grand->left != NULL ) {
		grand->left->parent = grand;
	}
#endif /* PARENT */

	return parent;
}

/* This function performs a right-left rotation on the red-black tree.
 * Node must be red and must also be the left child of parent.
 * Parent must be red and must also be the right child of grand.
 * Uncle must be black.
 * Grandparent must be black.
 * The function returns the new grandparent. */
static rb_node_t * __attribute__((nonnull))
rb_right_left_rotation ( rb_node_t * const grand,
                         rb_node_t * const parent,
                         rb_node_t * const node )
{
	assert(node->colour == RB_RED);
	assert(parent->colour == RB_RED);
	assert(grand->colour == RB_BLACK);
	assert(parent->left == node);
	assert(grand->right == parent);

	/* The uncle must be black. */
	assert(grand->left == NULL || grand->left->colour == RB_BLACK);

	node->colour = RB_BLACK;
	grand->colour = RB_RED;

	parent->left = node->right;
	grand->right = node->left;
	node->right = parent;
	node->left = grand;

#if PARENT
	node->parent = grand->parent;
	parent->parent = node;
	grand->parent = node;
	if ( parent->left != NULL ) {
		parent->left->parent = parent;
	}
	if ( grand->right != NULL ) {
		grand->right->parent = grand;
	}
#endif /* PARENT */

	return node;
}

/* This function will perform a left rotation on the red-black tree.
 * Node must be red and must also be the right child of parent.
 * Parent must be red and must also be the right child of grand.
 * Uncle must be black.
 * Grandparent must be black.
 * This function returns the new grandparent. */
static rb_node_t * __attribute__((nonnull))
rb_left_rotation ( rb_node_t * const grand,
                   rb_node_t * const parent )
{
	assert(parent->colour == RB_RED);
	assert(grand->colour == RB_BLACK);
	assert(grand->right == parent);

	/* Assert properties of the uncle and the node. */
	assert(parent->right->colour == RB_RED);
	assert(grand->left == NULL || grand->left->colour == RB_BLACK);

	grand->colour = RB_RED;
	parent->colour = RB_BLACK;

	grand->right = parent->left;
	parent->left = grand;

#if PARENT
	parent->parent = grand->parent;
	grand->parent = parent;
	if ( grand->right != NULL ) {
		grand->right->parent = grand;
	}
#endif /* PARENT */

	return parent;
}

/* This function performs a left-right rotation on the red-black tree.
 * Node must be red and must also be the right child of parent.
 * Parent must be red and must also be the left child of grand.
 * Uncle must be black.
 * Grandparent must be black.
 * This function returns the new grandparent. */
static rb_node_t * __attribute__((nonnull))
rb_left_right_rotation ( rb_node_t * const grand,
                         rb_node_t * const parent,
                         rb_node_t * const node )
{
	assert(node->colour == RB_RED);
	assert(parent->colour == RB_RED);
	assert(grand->colour == RB_BLACK);
	assert(parent->right == node);
	assert(grand->left == parent);

	/* The uncle must be black. */
	assert(grand->right == NULL || grand->right->colour == RB_BLACK);

	node->colour = RB_BLACK;
	grand->colour = RB_RED;

	parent->right = node->left;
	grand->left = node->right;
	node->left = parent;
	node->right = grand;

#if PARENT
	node->parent = grand->parent;
	grand->parent = node;
	parent->parent = node;
	if ( parent->right != NULL ) {
		parent->right->parent = parent;
	}
	if ( grand->left != NULL ) {
		grand->left->parent = grand;
	}
#endif /* PARENT */

	return node;
}

/* This function will perform necessary rotations on the red-black tree.
 * Node must be red.
 * Parent must be red.
 * Uncle must be black.
 * Grandparent must be black.
 * This function will return the new grandparent. */
static rb_node_t * __attribute__((nonnull))
rb_black_uncle_rotation ( rb_node_t * const grand,
                          rb_node_t * const parent,
                          rb_node_t * const node )
{
	assert(node->colour == RB_RED);
	assert(parent->colour == RB_RED);
	assert(grand->colour == RB_BLACK);

	assert(node == parent->left || node == parent->right);
	assert(parent == grand->left || parent == grand->right);

	if ( node == parent->left ) {
		if ( parent == grand->left ) {
			return rb_right_rotation(grand, parent);
		} else /* if ( parent == grand->right ) */ {
			return rb_right_left_rotation(grand, parent, node);
		}
	} else /* if ( node == parent->right ) */ {
		if ( parent == grand->left ) {
			return rb_left_right_rotation(grand, parent, node);
		} else /* if ( parent == grand->right ) */ {
			return rb_left_rotation(grand, parent);
		}
	}
}

#if PARENT
rb_error_t __attribute__((nonnull))
rb_insert ( rb_t * const rb,
            const int key,
            const int value )
{
	if ( rb->root == NULL ) {
		rb->root = rb_node_new(rb, key, value);
		if ( rb->root == NULL ) {
			return RB_NOMEM;
		}
		rb->root->colour = RB_BLACK;
		rb->root->parent = NULL;
		return RB_OK;
	}

	rb_node_t * parent = rb->root;
	rb_node_t * node;
	do {
		 if ( key < parent->key ) {
			if ( parent->left != NULL ) {
				parent = parent->left;
				continue;
			} else /* if ( parent->left == NULL ) */ {
				node = rb_node_new(rb, key, value);
				if ( node == NULL ) {
					return RB_NOMEM;
				}
				node->colour = RB_RED;
				parent->left = node;
				node->parent = parent;

				break;
			}
		} else if ( key > parent->key ) {
			if ( parent->right != NULL ) {
				parent = parent->right;
			} else /* if ( parent->right == NULL ) */ {
				node = rb_node_new(rb, key, value);
				if ( node == NULL ) {
					return RB_NOMEM;
				}
				node->colour = RB_RED;
				parent->right = node;
				node->parent = parent;

				break;
			}
		} else /* if ( key == parent->key ) */ {
			return RB_EXISTS;
		}
	} while ( true );

	/* The newly inserted node was red so if the parent is black no
	 * violation has been made. */
	if ( parent->colour == RB_BLACK ) {
		return RB_OK;
	}

	/* Now these two things are known:
	 *  1. The node is red.
	 *  2. The parent is red.
	 * The above are the conditions needed to enter this loop. */
	do {
		/* The grandparent is non-NULL because parent is red and the
		 * root is black so parent is not the root, thus atleast the
		 * root is left in the parent chain. Furthermore the grandparent
		 * must be black because parent is red. */
		rb_node_t * const grand = parent->parent;
		assert(grand != NULL);

		rb_node_t * const uncle = rb_node_get_uncle(grand, parent);
		if ( uncle == NULL || uncle->colour == RB_BLACK ) {
			rb_node_t * const great = grand->parent;
			rb_node_t * const new_grand =
				rb_black_uncle_rotation(grand, parent, node);
			if ( great != NULL ) {
				assert(grand == great->left || grand == great->right);
				if ( great->left == grand ) {
					great->left = new_grand;
				} else /* if ( great->right == grand ) */ {
					great->right = new_grand;
				}
			} else /* if ( great == NULL ) */ {
				rb->root = new_grand;
			}
			return RB_OK;
		}

		/* Just recolour the nodes. */
		parent->colour = RB_BLACK;
		uncle->colour = RB_BLACK;
		if ( grand != rb->root ) {
			grand->colour = RB_RED;
		} else /* if ( grand == rb->root ) */ {
			/* The grand is the root and should remain black. */
			return RB_OK;
		}

		/* The parent of the grand is non-NULL, because if grand is not
		 * the root atleast the root is in the parent chain. */
		parent = grand->parent;
		assert(parent != NULL);
		if ( parent->colour == RB_BLACK ) {
			/* If the new parent (the parent of the grand) is black
			 * everything is fine because the recoloured child (the
			 * former grand is red), so no violation is possible. */
			return RB_OK;
		}

		/* Save the grandparent as the node because it is needed for the
		 * next iteration. */
		node = grand;

		/* It is now prepared for the next iteration of the loop,
		 * because the status is the required, that is:
		 *  1. The node is red.
		 *  2. The parent is red. */
	} while ( true );
}
#elif RECURSIVE
/* Possible return values from the rb_node_insert function. */
enum rb_node_insert_return_e {
	RB_NODE_INSERT_OK = RB_OK,
	RB_NODE_INSERT_EXISTS = RB_EXISTS,
	RB_NODE_INSERT_NOMEM = RB_NOMEM,
	RB_NODE_INSERT_ROTATE_NEXT,
	RB_NODE_INSERT_ROTATE_NOW
};
typedef enum rb_node_insert_return_e rb_node_insert_return_t;

/* This function will rotate the tree.
 * Returns 0 if no more rotations are needed and 1 otherwise. */
static int __attribute__((nonnull(1,3,4,5)))
rb_rotate ( rb_t * const rb,
            rb_node_t * const great,
            rb_node_t * const grand,
            rb_node_t * const parent,
            rb_node_t * const node )
{
	assert(node->colour == RB_RED);
	assert(parent->colour == RB_RED);
	assert(grand->colour == RB_BLACK);

	rb_node_t * const uncle = rb_node_get_uncle(grand, parent);
	if ( uncle == NULL || uncle->colour == RB_BLACK ) {
		rb_node_t * const new_grand = rb_black_uncle_rotation(grand,
		                                                      parent,
		                                                      node);

		if ( great != NULL ) {
			assert(grand == great->left || grand == great->right);
			if ( grand == great->left ) {
				great->left = new_grand;
			} else /* if ( grand == great->right ) */ {
				great->right = new_grand;
			}
		} else /* if ( grand == NULL ) */ {
			assert(grand == rb->root);
			rb->root = new_grand;
		}

		/* No more rotations are needed. */
		return 0;
	}

	/* The uncle is red.  */
	parent->colour = RB_BLACK;
	uncle->colour = RB_BLACK;
	if ( great != NULL ) {
		grand->colour = RB_RED;
		if ( great->colour == RB_RED ) {
			/* Both great and grand are red, so further rotations are
			 * needed. */
			return 1;
		} else /* if ( great->colour == RB_BLACK ) */ {
			/* great is black and grand is red no, so there is no
			 * violation. */
			return 0;
		}
	} else /* if ( great == NULL ) */ {
		/* The grandparent must be the root and should remain black. */
		assert(grand == rb->root);

		/* No more rotations are needed. */
		return 0;
	}
}

static rb_node_insert_return_t __attribute__((nonnull(1,5)))
rb_node_insert ( rb_t * const rb,
                 rb_node_t * const great,
                 rb_node_t * const grand,
                 rb_node_t * const parent,
                 rb_node_t * const node,
                 const int key,
                 const int value )
{
	rb_node_insert_return_t ret;
	rb_node_t * new = NULL;
	if ( key < node->key ) {
		if ( node->left != NULL ) {
			ret = rb_node_insert(rb, grand, parent, node, node->left,
			                     key, value);
		} else {
			new = rb_node_new(rb, key, value);
			if ( new == NULL ) {
				return RB_NODE_INSERT_NOMEM;
			}
			node->left = new;
		}
	} else if ( key > node->key ) {
		if ( node->right != NULL ) {
			ret = rb_node_insert(rb, grand, parent, node, node->right,
			                     key, value);
		} else {
			new = rb_node_new(rb, key, value);
			if ( new == NULL ) {
				return RB_NODE_INSERT_NOMEM;
			}
			node->right = new;
		}
	} else /* if ( key == node->key ) */ {
		return RB_NODE_INSERT_EXISTS;
	}

	if ( new != NULL ) {
		new->colour = RB_RED;
		if ( node->colour == RB_BLACK ) {
			/* The insertion does not violate any of the rules. */
			return RB_NODE_INSERT_OK;
		}

		/* The new node is now red and node is red too. This violates
		 * (4).
		 * Because node is red it cannot be the root, as the root is
		 * black. Thus parent must not be NULL and black. */
		assert(parent != NULL);
		assert(parent->colour == RB_BLACK);

		if ( !rb_rotate(rb, grand, parent, node, new) ) {
			return RB_NODE_INSERT_OK;
		} else {
			return RB_NODE_INSERT_ROTATE_NOW;
		}
	}

	switch ( ret ) {
		case RB_NODE_INSERT_OK:
		case RB_NODE_INSERT_EXISTS:
		case RB_NODE_INSERT_NOMEM:
			return ret;
		case RB_NODE_INSERT_ROTATE_NEXT:
			/* The toplevel call to this function should not return
			 * RB_NODE_INSERT_ROTATE_NOW. */
			assert(parent != NULL);
			return RB_NODE_INSERT_ROTATE_NOW;
		case RB_NODE_INSERT_ROTATE_NOW:
			/* No rotation should be required of the toplevel function
			 * call. */
			assert(parent != NULL);
			break;
	}

	/* The tree must be rotated. */
	assert(node->colour == RB_RED);
	assert(parent->colour == RB_RED);
	if ( !rb_rotate(rb, great, grand, parent, node) ) {
		return  RB_NODE_INSERT_OK;
	} else {
		return RB_NODE_INSERT_ROTATE_NEXT;
	}
}

rb_error_t __attribute__((nonnull))
rb_insert ( rb_t * const rb, const int key, const int value )
{
	if ( rb->root == NULL ) {
		rb_node_t * const new = rb_node_new(rb, key, value);
		if ( new == NULL ) {
			return RB_NOMEM;
		}
		new->colour = RB_BLACK;
		rb->root = new;
		return RB_OK;
	}

	return rb_node_insert(rb, NULL, NULL, NULL, rb->root, key, value);
}
#elif STACK
/* This function will update the great-grandparent from having grand as
 * child to have new_grand as child. */
static void __attribute__((nonnull))
rb_update_great ( rb_t * const rb,
                  rb_node_t * const grand,
                  rb_node_t * const new_grand )
{
	if ( grand == rb->root ) {
		rb->root = new_grand;
		return;
	}

	rb_node_t * const great = rb_stack_pop(rb);

	/* Update the great-grandparent. */
	assert(great != NULL);
	assert(grand == great->left || grand == great->right);
	if ( great->left == grand ) {
		great->left = new_grand;
	} else /* if ( great->right == grand ) */ {
		great->right = new_grand;
	}
	return;
}

rb_error_t __attribute__((nonnull))
rb_insert ( rb_t * const rb,
            const int key,
            const int value )
{
	/* Insert the new node as the root if there is no root. */
	if ( rb->root == NULL ) {
		rb_node_t * const node = rb_node_new(rb, key, value);
		if ( node == NULL ) {
			return RB_NOMEM;
		}
		node->colour = RB_BLACK;
		rb->root = node;
		return RB_OK;
	}

	rb_stack_clear(rb);

	rb_node_t * grand = NULL;
	rb_node_t * parent = rb->root;
	rb_node_t * node;
	do {
		if ( key < parent->key ) {
			/* The new node should be inserted to the left of the
			 * parent. */
			if ( parent->left != NULL ) {
				/* Push the old grandparent to the stack. */
				if ( rb_stack_push(rb, grand) ) {
					return RB_NOMEM;
				}

				grand = parent;
				parent = parent->left;
				continue;
			} else /* if ( parent->left == NULL ) */ {
				/* The node should be inserted here. */
				node = rb_node_new(rb, key, value);
				if ( node == NULL ) {
					return RB_NOMEM;
				}
				node->colour = RB_RED;
				parent->left = node;

				break;
			}
		} else if ( key > parent->key ) {
			/* The new node should be inserted to the right of the
			 * parent. */
			 if ( parent->right != NULL ) {
				if ( rb_stack_push(rb, grand) ) {
					return RB_NOMEM;
				}

				grand = parent;
				parent = parent->right;
				continue;
			} else /* if ( parent->right == NULL ) */ {
				/* The node should be inserted here. */
				node = rb_node_new(rb, key, value);
				if ( node == NULL ) {
					return RB_NOMEM;
				}
				node->colour = RB_RED;
				parent->right = node;

				break;
			}
		} else /* if ( key == parent->key ) */ {
			return RB_EXISTS;
		}
	} while ( true );

	/* The newly inserted node is red so if the parent is black no
	 * violation has been made. */
	if ( parent->colour == RB_BLACK ) {
		return RB_OK;
	}

	/* Now three things are known:
	 *  1. The node is red.
	 *  2. The parent is red.
	 *  3. The grandparent is black.
	 * The above are the conditions needed to enter this loop. */
	do {
		rb_node_t * const uncle = rb_node_get_uncle(grand, parent);
		if ( uncle == NULL || uncle->colour == RB_BLACK ) {
			rb_node_t * const new_grand =
				rb_black_uncle_rotation(grand, parent, node);
			rb_update_great(rb, grand, new_grand);
			return RB_OK;
		}

		/* Just repaint the nodes. */
		parent->colour = RB_BLACK;
		uncle->colour = RB_BLACK;
		if ( grand != rb->root ) {
			grand->colour = RB_RED;
		} else /* if ( grand == rb->root ) */ {
			/* The grandparent is the root and should remain black. */
			return RB_OK;
		}

		/* rb_stack_pop() will return non-NULL for the parent, because
		 * if grandparent was the root (and there are no nodes on the
		 * stack) this code will not be reached. Moreover holds that if
		 * the grandparent was not root then atleast the root is in the
		 * stack. */
		parent = rb_stack_pop(rb);
		assert(parent != NULL);
		if ( parent->colour == RB_BLACK ) {
			/* If the parent (of the grandparent) is black everything is
			 * fine because the recoloured child (the former grandparent
			 * is red), so no violation is possible. */
			return RB_OK;
		}

		/* Save the grandparent as the node because it is needed for the
		 * next iteration. */
		node = grand;

		/* rb_stack_pop() will return non-NULL for the grandparent,
		 * because parent is red and the root is black so parent is not
		 * the root, thus atleast the root is left.
		 * Furthermore the grandparent must be black because parent is
		 * red. */
		grand = rb_stack_pop(rb);
		assert(grand != NULL);

		/* It is now prepared for the next iteration of the loop,
		 * because the status is the required, that is:
		 *  1. The node is red.
		 *  2. The parent is red.
		 *  3. The grandparent is black and might be the root. */
	} while ( true );
}
#endif /* STACK */

int __attribute__((nonnull))
rb_search ( const rb_t * const rb,
            const int key,
            int * const valuep )
{
	const rb_node_t * node = rb->root;
	while ( node != NULL ) {
		if ( key > node->key ) {
			node = node->right;
		} else if ( key < node->key ) {
			node = node->left;
		} else /* if ( key == node->key ) */ {
			*valuep = node->value;
			return 0;
		}
	}
	return 1;
}

/* This function returns a string containing the name of the given
 * colour. */
static const char * __attribute__((const))
rb_strcolour ( const rb_colour_t colour )
{
	assert(colour == RB_BLACK || colour == RB_RED);
	return (colour == RB_BLACK) ? "black" : "red";
}

static void __attribute__((nonnull))
rb_sanity_check_fail ( const rb_node_t * const node,
                       const size_t n_black_left,
                       const size_t n_black_right,
                       const size_t depth )
{
	fprintf(stderr,
	        "RB sanity check failed at:\n"
	        "  address:     %p\n"
	        "  key:         %d\n"
	        "  value:       %d\n"
	        "  depth:       %lu\n"
	        "  colour:      %s\n",
	        (void *)node, node->key, node->value, depth,
	        rb_strcolour(node->colour));
	if ( n_black_left == SIZE_MAX ) {
		fputs("  black left:  unknown\n", stderr);
	} else {
		fprintf(stderr, "  black left:  %lu\n", n_black_left);
	}
	if ( n_black_right == SIZE_MAX ) {
		fputs("  black right: unknonw\n", stderr);
	} else {
		fprintf(stderr, "  black right: %lu\n", n_black_right);
	}

	rb_node_t * const left = node->left;
	if ( left == NULL ) {
		fputs("  left node: NULL\n", stderr);
	} else {
		fputs("  left node:\n", stderr);
#if PARENT
		fprintf(stderr, "    parent:  %p\n", (void *)left->parent);
#endif /* PARENT */
		fprintf(stderr,
		        "    address: %p\n"
		        "    key:     %d\n"
		        "    value:   %d\n"
		        "    colour:  %s\n",
		        (void *)left, left->key, left->value,
		        rb_strcolour(left->colour));
	}

	rb_node_t * const right = node->right;
	if ( right == NULL ) {
		fputs("  right node: NULL\n", stderr);
	} else {
		fputs("  right node:\n", stderr);
#if PARENT
		fprintf(stderr, "    parent:  %p\n", (void *)right->parent);
#endif /* PARENT */
		fprintf(stderr,
		        "    address: %p\n"
		        "    key:     %d\n"
		        "    value:   %d\n"
		        "    colour:  %s\n",
		        (void *)right, right->key, right->value,
		        rb_strcolour(right->colour));
	}

	return;
}

/* Runs sanity checks on the node recursively.
 * The variable at n_black will be filled in with the number of black
 * nodes from this node (inclusive) to any leaf node (exclusive).
 * Returns 0 on if the test succeeded or 1 otherwise. */
static int __attribute__((nonnull))
rb_node_check_recursive ( const rb_node_t * const node,
                          const size_t depth,
                          size_t * const n_black )
{
	size_t n_black_left = SIZE_MAX;
	size_t n_black_right = SIZE_MAX;

	const rb_node_t * const left = node->left;
	if ( left != NULL ) {
		/* Test for the general property of binary trees. */
		if ( node->key <= left->key ) {
			rb_sanity_check_fail(node, n_black_left, n_black_right,
			                     depth);
			return 1;
		}

#if PARENT
		if ( node != left->parent ) {
			rb_sanity_check_fail(node, n_black_left, n_black_right,
			                     depth);
			return 1;
		}
#endif /* PARENT */

		/* Test for property (4). */
		if ( node->colour == RB_RED && left->colour == RB_RED ) {
			rb_sanity_check_fail(node, n_black_left, n_black_right,
			                     depth);
			return 1;
		}

		if ( rb_node_check_recursive(left, depth + 1, &n_black_left) ) {
			return 1;
		}
	} else {
		n_black_left = 0;
	}

	const rb_node_t * const right = node->right;
	if ( right != NULL ) {
		/* Test for the general property of binary trees. */
		if ( node->key >= right->key ) {
			rb_sanity_check_fail(node, n_black_left, n_black_right,
			                     depth);
			return 1;
		}

#if PARENT
		if ( node != right->parent ) {
			rb_sanity_check_fail(node, n_black_left, n_black_right,
			                     depth);
			return 1;
		}
#endif /* PARENT */

		/* Test for property (4). */
		if ( node->colour == RB_RED && right->colour == RB_RED ) {
			rb_sanity_check_fail(node, n_black_left, n_black_right,
			                     depth);
			return 1;
		}

		if ( rb_node_check_recursive(right, depth + 1, &n_black_right) )
		{
			return 1;
		}
	} else {
		n_black_right = 0;
	}


	/* Test for property (5). */
	if ( n_black_left != n_black_right ) {
		rb_sanity_check_fail(node, n_black_left, n_black_right, depth);
		return 1;
	}

	if ( node->colour == RB_BLACK ) {
		*n_black = n_black_left + 1;
	} else /* if ( rb->colour == RB_RED ) */ {
		*n_black = n_black_left;
	}

	return 0;
}

int __attribute__((nonnull))
rb_check ( const rb_t * const rb )
{
	if ( rb->root != NULL ) {
		/* Check for property (2). */
		if ( rb->root->colour != RB_BLACK ) {
			rb_sanity_check_fail(rb->root, SIZE_MAX, SIZE_MAX, 0);
			return 1;
		}

		size_t height;
		return rb_node_check_recursive(rb->root, 0, &height);
	} else {
		return 0;
	}
}
